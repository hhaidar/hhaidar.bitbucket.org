'use strict';

+function(window, $) {

    $(window).on('playvideo', function() {

        $('<iframe />')
            .attr({
                id: 'video',
                src: 'https://drive.google.com/file/d/0B9wAYkH1ZI1zbGo3eU5WamtraU0/preview?autoplay=1',
                width: '100%',
                height: '100%'
            })
            .appendTo('#container');

    });

}(window, jQuery);
